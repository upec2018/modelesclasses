
import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


public class Story extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private int priority;

    private Date startDate;

    private Date endDate;
    private String acceptance;
    private int estimation;

    private Sprint sprint;

    private List<Task> tasks;

    public Story() {
        this.startDate = new Date();
    }

    public Story(String name) {
        this();
        this.name = name;
    }

    public Story(String name, Sprint sprint) {
        this(name);
        this.name = name;
        if (sprint != null) {
            sprint.addStory(this);
        }
    }

    public String getAcceptance() {
        return acceptance;
    }

    public void setAcceptance(String acceptance) {
        this.acceptance = acceptance;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getEstimation() {
        return estimation;
    }

    public void setEstimation(int estimation) {
        this.estimation = estimation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public List<Task> getTasks() {
        return (tasks != null) ? Collections.unmodifiableList(tasks) : Collections.EMPTY_LIST;
    }

    public List<Task> getDoneTasks() {
        return Collections.unmodifiableList(getTasks(TaskStatus.DONE));
    }

    public List<Task> getWorkingTasks() {
        return Collections.unmodifiableList(getTasks(TaskStatus.WORKING));
    }

    public List<Task> getTodoTasks() {
        return Collections.unmodifiableList(getTasks(TaskStatus.TODO));
    }

    private List<Task> getTasks(TaskStatus status) {
        List<Task> result = new LinkedList<Task>();
        if (tasks != null && !tasks.isEmpty()) {
            for (Task task : tasks) {
                if (task != null && status.equals(task.getStatus())) {
                    result.add(task);
                }
            }
        }
        return result;
    }

    public boolean addTask(Task task) {
        if (tasks == null) {
            tasks = new LinkedList<Task>();
        }
        if (task != null && !tasks.contains(task)) {
            tasks.add(task);
            task.setStory(this);
            return true;
        }
        return false;
    }

    public boolean removeTask(Task task) {
        if (tasks != null && !tasks.isEmpty()) {
            return tasks.remove(task);
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Story other = (Story) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if (this.sprint != other.sprint && (this.sprint == null || !this.sprint.equals(other.sprint))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 23 * hash + (this.sprint != null ? this.sprint.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Story[name=" + name + ",startDate=" + startDate + ",sprint=" + sprint + "]";
    }
}
