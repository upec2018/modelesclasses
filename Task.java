

package com.upec.m2.jpa;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


public class Task extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;


    private Date startDate;


    private Date endDate;


    private TaskStatus status;


    private Story story;

    public Task() {
        this.status = TaskStatus.TODO;
    }

    public Task(String name) {
        this();
        this.name = name;
    }

    public Task(String name, Story story) {
        this(name);
        if (story != null) {
            story.addTask(this);
        }
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
        changeTaskStatus(this.startDate, endDate);

    }

    protected void changeTaskStatus(Date startDate, Date endDate) {
        if (endDate != null) {
            this.setStatus(status.DONE);
        }
        if (endDate == null && this.startDate != null) {
            this.setStatus(status.WORKING);
        }
        if (endDate == null && this.startDate == null) {
            this.setStatus(status.TODO);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
        changeTaskStatus(startDate, this.endDate);
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public Story getStory() {
        return story;
    }

    public void setStory(Story story) {
        this.story = story;
    }

    public String getStatusKeyI18n() {
        return "task.show.table.header.status."+status;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Task other = (Task) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if (this.story != other.story && (this.story == null || !this.story.equals(other.story))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 83 * hash + (this.story != null ? this.story.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Task[name=" + name + ",startDate=" + startDate + ",story=" + story + "]";
    }
}
