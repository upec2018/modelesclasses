

package com.upec.m2.jpa;

import java.io.Serializable;


public interface PersistentEntity<PK extends Serializable> extends Serializable {

    PK getId();

    boolean isNew();
    
}
