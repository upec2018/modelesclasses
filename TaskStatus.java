

package com.upec.m2.jpa;


public enum TaskStatus {

    TODO, DONE, WORKING
}
