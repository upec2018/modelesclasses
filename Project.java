

package com.upec.m2.jpa;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


public class Project extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    private Date startDate;

    private Date endDate;

    private List<Sprint> sprints;

    public Project() {
        this.startDate = new Date();
    }

    public Project(String name) {
        this();
        this.name = name;
    }

    public Project(String name, Date startDate) {
        this(name);
        this.startDate = startDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<Sprint> getSprints() {
        return (sprints != null) ? Collections.unmodifiableList(sprints) : Collections.EMPTY_LIST;
    }

    public boolean addSprint(Sprint sprint) {
        if (sprints == null) {
            sprints = new LinkedList<Sprint>();
        }
        if (sprint != null && !sprints.contains(sprint)) {
            sprints.add(sprint);
            sprint.setProject(this);
            return true;
        }
        return false;
    }

    public boolean removeSpring(Sprint sprint) {
        if (sprints != null && !sprints.isEmpty()) {
            return sprints.remove(sprint);
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Project other = (Project) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Project[name=" + name + ",startDate=" + startDate + ",endDate=" + endDate + "]";
    }
}
