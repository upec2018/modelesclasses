

package com.upec.m2.jpa;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


public class Sprint extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String goals;

    private Date startDate;

    private Date endDate;

    private int iterationScope;

    private int gainedStoryPoints;

    private Date dailyMeetingTime;

    private List<Story> stories;

    private Project project;

    public Sprint() {
        this.startDate = new Date();
    }

    public Sprint(String name) {
        this();
        this.name = name;
    }

    public Sprint(String name, Project project) {
        this(name);
        this.project = project;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGoals() {
        return goals;
    }

    public void setGoals(String goals) {
        this.goals = goals;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getIterationScope() {
        return iterationScope;
    }

    public void setIterationScope(int iterationScope) {
        this.iterationScope = iterationScope;
    }

    public int getGainedStoryPoints() {
        return gainedStoryPoints;
    }

    public void setGainedStoryPoints(int gainedStoryPoints) {
        this.gainedStoryPoints = gainedStoryPoints;
    }

    public Date getDailyMeetingTime() {
        return dailyMeetingTime;
    }

    public void setDailyMeetingTime(Date dailyMeetingTime) {
        this.dailyMeetingTime = dailyMeetingTime;
    }

    public List<Story> getStories() {
        return (stories != null) ? Collections.unmodifiableList(stories) : Collections.EMPTY_LIST;
    }

    public boolean addStory(Story story) {
        if (stories == null) {
            stories = new LinkedList<Story>();
        }
        if (story != null && !stories.contains(story)) {
            stories.add(story);
            story.setSprint(this);
            return true;
        }
        return false;
    }

    public boolean removeStory(Story story) {
        if (stories != null && !stories.isEmpty()) {
            return stories.remove(story);
        } else {
            return false;
        }
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sprint other = (Sprint) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if (this.project != other.project && (this.project == null || !this.project.equals(other.project))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 97 * hash + (this.project != null ? this.project.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Sprint[name=" + name + ",startDate=" + startDate + ",project=" + project + "]";
    }
}
